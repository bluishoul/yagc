import React, { useState, useEffect } from "react";
import { Box, Grid, Text, Image } from "@chakra-ui/core";
import { initialized } from "../lib/graphql-api";

const Home = () => {
  const [enterprise, setEnterprise] = useState({ enterprises: [] });
  useEffect(() => {
    if (enterprise.enterprises.length === 0) {
      initialized().then((data) => {
        setEnterprise(data);
      });
    }
  });
  return (
    <Box m="auto" w={{ base: "100%", xl: "1200px" }} minH="100%">
      <Box
        borderWidth={{ base: 0, xl: 1 }}
        rounded={{ base: 0, xl: "lg" }}
        p={{ base: 0, xl: 4 }}
        mt={{ base: 0, xl: 4 }}
      >
        <Grid
          templateColumns={{ lg: "repeat(2, 1fr)", xl: "repeat(3, 1fr)" }}
          gap={2}
        >
          {(enterprise.enterprises || []).map((e) => (
            <Box>
              <Box
                display="flex"
                flexDir="column"
                alignItems="center"
                borderWidth={{ lg: 1 }}
                rounded={{ lg: "lg" }}
                py="4"
              >
                <Image src={e.avatar_url} alt={e.name} w={40} />
                <Text mt={2} color="gray.500">
                  {e.name}
                </Text>
              </Box>
            </Box>
          ))}
        </Grid>
      </Box>
    </Box>
  );
};

export default Home;
