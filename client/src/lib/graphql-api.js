import { GraphQLClient } from "graphql-request";

const client = new GraphQLClient("/graphql");

export const initialized = async () => {
  const query = `{
        enterprises {
            id,
            name,
            avatar_url,
        }
    }`;
  return await client.request(query);
};
