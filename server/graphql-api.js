const { enterprises: dbEnterprises } = require("./dao");

const query = `
    type Query {
        enterprises: [Enterprise]
    }
`;

const enterprises = (db) =>
  async function () {
    return await dbEnterprises.queryAll(db);
  };

module.exports = {
  query,
  enterprises,
};
