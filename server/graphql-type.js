module.exports = `
    type User {
        id: ID
        name: String
        avatar_url: String
    }
    
    type Enterprise {
        id: ID
        name: String
        avatar_url: String
        owner: User
    }
`;
