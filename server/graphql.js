const graphqlHTTP = require("express-graphql");
const { buildSchema } = require("graphql");
const type = require("./graphql-type");
const { query, ...api } = require("./graphql-api");
const schema = buildSchema([type, query].join("\n"));

module.exports = function (db) {
  const boundApi = Object.keys(api || []).reduce(
    (previous, key) => ({
      ...previous,
      [key]: api[key](db),
    }),
    {}
  );
  return graphqlHTTP({
    schema: schema,
    rootValue: { ...boundApi },
    graphiql: true,
  });
};
