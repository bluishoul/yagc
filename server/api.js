const { get } = require("./request");
const { normalize } = require("normalizr");
const { enterprise } = require("./model");

async function enterpriseList(accessToken) {
  const { total_count, data } = await get("/enterprises/list", accessToken, {
    direction: "desc",
    page: 1,
  });
  return normalize(data, [enterprise]);
}

async function enterprisePrograms(accessToken, id) {
  const { total_count, data } = await get(
    `/enterprises/${id}/programs`,
    accessToken,
    {
      page: 1,
      per_page: 100,
    }
  );
}

module.exports = {
  enterprise: {
    list: enterpriseList,
    programs: enterprisePrograms,
  },
};
