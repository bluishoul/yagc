const winston = require("winston");
const service = require("./package.json").name;

const logger = winston.createLogger({
  format: winston.format.json(),
  defaultMeta: { service },
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({
      filename: `${service}-error.log`,
      level: "error",
    }),
    new winston.transports.File({ filename: `${service}-combined.log` }),
  ],
});

module.exports = logger;
