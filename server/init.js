const { enterprise } = require("./api");
const { enterprises: dbEnterprises } = require("./dao");

module.exports = async function init(session, db) {
  const enterprises = await enterprise.list(
    session.grant.response.access_token
  );
  const saved = await dbEnterprises.addAll(
    db,
    Object.values(enterprises.entities.enterprises)
  );
  return saved;
};
