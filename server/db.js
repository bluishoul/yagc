const assert = require("assert");
const MongoClient = require("mongodb").MongoClient;
const { MONGODB_URL, MONGODB_DB_NAME = "yagc" } = process.env;

const newClient = () =>
  new MongoClient(MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

const connect = async (client) => {
  await client.connect();
  return client.db(MONGODB_DB_NAME);
};

module.exports = {
  newClient,
  connect,
};
