const { schema } = require("normalizr");

const user = new schema.Entity("users");

const enterprise = new schema.Entity("enterprises", {
  owner: user,
});

const program = new schema.Entity("programs", {
  assignee: user,
});

module.exports = {
  user,
  enterprise,
  program,
};
