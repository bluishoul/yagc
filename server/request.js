const fetch = require("node-fetch");
const q = require("querystring");

const { GITEE_API_HOST = "https://api.gitee.com" } = process.env;

const HEADERS = { "Content-Type": "application/json;charset=UTF-8" };

async function get(url, access_token, params) {
  const query = q.stringify({
    access_token,
    ...params,
  });
  const fullUrl = `${GITEE_API_HOST}${url}?${query}`;
  const res = await fetch(fullUrl, {
    headers: HEADERS,
  });
  return await res.json();
}

module.exports = {
  get,
};
